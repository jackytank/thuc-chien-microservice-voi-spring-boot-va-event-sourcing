<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>ShangHai</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"
		integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous"
		referrerpolicy="no-referrer"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.16/jstree.min.js"
		integrity="sha512-ekwRoEshEqHU64D4luhOv/WNmhml94P8X5LnZd9FNOiOfSKgkY12cDFz3ZC6Ws+7wjMPQ4bPf94d+zZ3cOjlig==" crossorigin="anonymous"
		referrerpolicy="no-referrer"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
		integrity="sha512-z3gLpd7yknf1YoNbCzqRKc4qyor8gaKU1qmn+CShxbuBusANI9QpRohGBreCFkKxLhei6S9CQXFEbbKuqLg0DA==" crossorigin="anonymous"
		referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css" />
</head>

<body>
	<h1>JSTREE Page</h1>
    <a href="/jakartaee-hello-world/"><= go back </a>
    <ul>
        <c:forEach var="item" items="${mockArray}">
            <li>${item}</li>
        </c:forEach>
    </ul>

	<!-- <h2>Demo1</h2>
	<div id="demo1"></div>

	<br>

	<h2>Demo2</h2>
	<div id="demo2"></div>

	<br>

	<h2>Demo3</h2>
	<div id="demo3"></div>

	<br> -->

	<!-- <h2>Demo4 (JAX-RS API Call)</h2>
	<div id="demo4"></div>

	<br>

	<h2>Demo5 (JAX-RS API Call)</h2>
	<div id="demo5"></div>

	<br> -->

	<h2>Demo6 (Servlet API Call)</h2>
	<div id="demo6"></div>

	<br>

	<h2>Demo7 (Servlet API Call)</h2>
	<div id="demo7"></div>

	<script>
		$('#demo1').jstree({
			'core': {
				'data': [
					{ "id": "ajson1", "parent": "#", "text": "Simple root node" },
					{ "id": "ajson2", "parent": "#", "text": "Root node 2" },
					{ "id": "ajson3", "parent": "ajson2", "text": "Child 1" },
					{ "id": "ajson4", "parent": "ajson2", "text": "Child 2" },
				]
			}
		});

		$('#demo2').jstree({
			'core': {
				'data': [
					'Root Node 1',
					{
						'text': 'Root node 2',
						'state': {
							'opened': true,
							'selected': true
						},
						'children': [
							{
								'text': 'Recipe',
								'state': {
									'opened': true,
								},
								'children': [
									{
										'text': 'Inner 1',
										'children': [
											{
												'text': 'Inner 1'
											},
											{
												'text': 'Inner 2',
												'children': [
													{
														'text': 'Inner 1'
													},
													{
														'text': 'Inner 2'
													}
												],
												'state': {
													'opened': true,
												},
											}
										],
										'state': {
											'opened': true,
										},
									},
									{
										'text': 'Inner 2'
									}
								]
							},
							{
								'text': 'Equipment'
							}
						]
					}
				]
			}
		});

		$("#demo3").jstree({
			core: {
				data: [
					{
						text: "Animals",
						icon: "fa fa-paw",
						'state': {
							'opened': true,
						},
						children: [
							{
								text: "Mammals",
								icon: "fa fa-dog",
								'state': {
									'opened': true,
								},
								children: [
									{
										text: "Dogs",
										icon: "fa fa-dog",
										'state': {
											'opened': true,
										},
										children: [
											{
												text: "Labrador",
												'state': {
													'opened': true,
												},
												icon: "fa fa-dog"
											},
											{
												text: "Poodle",
												'state': {
													'opened': true,
												},
												icon: "fa fa-dog"
											},
											{
												text: "German Shepherd",
												'state': {
													'opened': true,
												},
												icon: "fa fa-dog"
											},
										],
									},
									{
										text: "Cats",
										icon: "fa fa-cat",
										'state': {
											'opened': true,
										},
										children: [
											{ text: "Persian", icon: "fa fa-cat" },
											{ text: "Siamese", icon: "fa fa-cat" },
											{ text: "Bengal", icon: "fa fa-cat" },
										],
									},
									{
										text: "Bears",
										icon: "fa fa-bear",
										'state': {
											'opened': true,
										},
										children: [
											{ text: "Polar", icon: "fa fa-bear" },
											{ text: "Grizzly", icon: "fa fa-bear" },
											{ text: "Panda", icon: "fa fa-bear" },
										],
									},
								],
							},
							{
								text: "Birds",
								icon: "fa fa-feather-alt",
								'state': {
									'opened': true,
								},
								children: [
									{
										text: "Parrots",
										icon: "fa fa-feather-alt",
										'state': {
											'opened': true,
										},
										children: [
											{ text: "Macaw", icon: "fa fa-feather-alt" },
											{ text: "Cockatoo", icon: "fa fa-feather-alt" },
											{ text: "Budgie", icon: "fa fa-feather-alt" },
										],
									},
									{
										text: "Eagles",
										icon: "fa fa-feather-alt",
										'state': {
											'opened': true,
										},
										children: [
											{ text: "Bald", icon: "fa fa-feather-alt" },
											{ text: "Golden", icon: "fa fa-feather-alt" },
											{ text: "Harpy", icon: "fa fa-feather-alt" },
										],
									},
									{
										text: "Penguins",
										icon: "fa fa-feather-alt",
										'state': {
											'opened': true,
										},
									}
								]
							}
						]
					}
				]
			}
		});

		$(function () {
			$("#demo4").jstree({
				core: {
					data: {
						url: "http://localhost:8080/jakartaee-hello-world/api/v1/jstree-data/js-tree-1",
						dataType: "json"
					}
				}
			});
		});

		
		$(function () {
			$("#demo5").jstree({
				core: {
					data: {
						url: "http://localhost:8080/jakartaee-hello-world/api/v1/jstree-data/js-tree-2",
						dataType: "json"
					}
				}
			});
		});

		$(function () {
			$("#demo6").jstree({
				core: {
					data: {
						url: "/jakartaee-hello-world/api/v1/servlet/jstree-data/js-tree-1/",
						dataType: "json"
					}
				}
			});
		});
		
		$(function () {
			$("#demo7").jstree({
				core: {
					data: {
						url: "/jakartaee-hello-world/api/v1/servlet/jstree-data/js-tree-2/",
						dataType: "json"
					}
				}
			});
		});

	</script>
</body>

</html>