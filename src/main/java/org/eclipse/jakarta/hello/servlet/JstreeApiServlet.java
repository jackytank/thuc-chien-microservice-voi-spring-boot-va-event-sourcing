package org.eclipse.jakarta.hello.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jakarta.hello.jaxrs.jstree.Node;
import org.eclipse.jakarta.hello.jaxrs.jstree.NodeService;

import com.google.gson.Gson;

@WebServlet("/api/v1/servlet/jstree-data/*")
public class JstreeApiServlet extends HttpServlet {
    private NodeService nodeService;

    @Override
    public void init() throws ServletException {
        nodeService = new NodeService();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException {
        response.setContentType("application/json");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
        try (PrintWriter out = response.getWriter()) {
            String path = request.getPathInfo();
            if (path == null || path.equals("/")) {
                List<Node> nodesForJsTree1 = nodeService.getNodesForJsTree1();
                List<Node> nodesForJsTree2 = nodeService.getNodesForJsTree2();
                Map<String, List<Node>> result = new HashMap<>();
                result.put("js-tree-1", nodesForJsTree1);
                result.put("js-tree-2", nodesForJsTree2);
                out.println(new Gson().toJson(result));
            } else if (path.startsWith("/js-tree-1")) {
                List<Node> nodesForJsTree1 = nodeService.getNodesForJsTree1();
                out.println(new Gson().toJson(nodesForJsTree1));
            } else if (path.startsWith("/js-tree-2")) {
                List<Node> nodesForJsTree2 = nodeService.getNodesForJsTree2();
                out.println(new Gson().toJson(nodesForJsTree2));
            } else {
                response.sendError(HttpServletResponse.SC_NOT_FOUND, "Invalid path: " + path);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            doGet(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
