import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/jstree-page")
public class JstreeServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<String> mockArray = createMockArray(); // Create a mock array
        request.setAttribute("mockArray", mockArray); // Set the mock array as a request attribute
        request.getRequestDispatcher("/jstree.jsp").forward(request, response); // Forward to index.jsp
    }

    private List<String> createMockArray() {
        List<String> array = new ArrayList<>();
        array.add("Mock Item 1");
        array.add("Mock Item 2");
        array.add("Mock Item 3");
        return array;
    }
}