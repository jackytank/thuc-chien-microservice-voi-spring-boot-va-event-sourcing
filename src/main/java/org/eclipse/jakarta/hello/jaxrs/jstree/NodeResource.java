// package org.eclipse.jakarta.hello.jaxrs.jstree;

// import java.util.List;

// import javax.inject.Inject;
// import javax.ws.rs.GET;
// import javax.ws.rs.Path;
// import javax.ws.rs.Produces;
// import javax.ws.rs.core.MediaType;

// @Path("/api/v1/jstree-data")
// public class NodeResource {
//     @Inject
//     private NodeService nodeService;

//     @GET
//     @Path("/js-tree-1")
//     @Produces({ MediaType.APPLICATION_JSON })
//     public List<Node> getNodesForJsTree1() {
//         return nodeService.getNodesForJsTree1();
//     }
    
//     @GET
//     @Path("/js-tree-2")
//     @Produces({ MediaType.APPLICATION_JSON })
//     public List<Node> getNodesForJsTree2() {
//         return nodeService.getNodesForJsTree2();
//     }
// }
