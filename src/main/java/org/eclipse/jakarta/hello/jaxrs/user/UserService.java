package org.eclipse.jakarta.hello.jaxrs.user;

import javax.enterprise.context.ApplicationScoped;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ApplicationScoped
public class UserService {
    private HashMap<Integer, User> users;

    public UserService() {
        users = new HashMap<>();
        // initialize with base data
        for (int i = 0; i < 100; i++) {
            User user = User.builder()
                    .id(i)
                    .username("user" + i)
                    .password("password"+i)
                    .isAdmin(i % 2 == 0)
                    .build();
            users.put(user.getId(), user);
        }
    }
    public List<User> getUsersList() {
        return new ArrayList<>(users.values());
    }
    public void addUser(User user) {
        users.put(user.getId(), user);
    }

    public User getUser(Integer id) {
        return users.get(id);
    }

    public void updateUser(User user) {
        users.put(user.getId(), user);
    }

    public void deleteUser(Integer id) {
        users.remove(id);
    }
}