package org.eclipse.jakarta.hello.jaxrs.user;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/api/v1/users")
public class UserResource {

    @Inject
    private UserService userService;

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<User> getUsers() {
        return userService.getUsersList();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public User getUser(@PathParam("id") Integer id) {
        return userService.getUser(id);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User addUser(User user) {
        userService.addUser(user);
        return user;
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User updateUser(@PathParam("id") Integer id, User user) {
        user.setId(id);
        userService.updateUser(user);
        return user;
    }

    @DELETE
    @Path("/{id}")
    public void deleteUser(@PathParam("id") Integer id) {
        userService.deleteUser(id);
    }
}
