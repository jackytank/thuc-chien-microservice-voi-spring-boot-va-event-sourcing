package org.eclipse.jakarta.hello.jaxrs.jstree;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class Node {
  private String id;
  private String text;
  private List<Node> children;

  public Node(String id, String text) {
    this.id = id;
    this.text = text;
    this.children = new ArrayList<>();
  }

  public void addChild(Node child) {
    this.children.add(child);
  }
}
