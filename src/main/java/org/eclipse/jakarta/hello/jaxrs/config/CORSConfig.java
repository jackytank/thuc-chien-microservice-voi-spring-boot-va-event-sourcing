// package org.eclipse.jakarta.hello.jaxrs.config;

// import java.io.IOException;

// import javax.ws.rs.container.ContainerRequestContext;
// import javax.ws.rs.container.ContainerResponseContext;
// import javax.ws.rs.container.ContainerResponseFilter;
// import javax.ws.rs.ext.Provider;

// @Provider
// public class CORSConfig implements ContainerResponseFilter {

//     @Override
//     public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
//             throws IOException {
//         // get the request origin
//         String origin = requestContext.getHeaderString("Origin");
//         // add the CORS headers to the response
//         responseContext.getHeaders().add("Access-Control-Allow-Origin", origin);
//         responseContext.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
//         responseContext.getHeaders().add("Access-Control-Allow-Headers", "Content-Type, Accept, Authorization");
//         responseContext.getHeaders().add("Access-Control-Allow-Credentials", "true");
//     }
// }
