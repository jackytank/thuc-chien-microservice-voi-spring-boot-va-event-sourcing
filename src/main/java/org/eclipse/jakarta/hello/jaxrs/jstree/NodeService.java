package org.eclipse.jakarta.hello.jaxrs.jstree;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class NodeService {

    public List<Node> getNodes() {
        List<Node> nodes = new ArrayList<>();

        Node node1 = new Node("node1", "Node 1");
        Node node11 = new Node("node11", "Node 1.1");
        Node node12 = new Node("node12", "Node 1.2");
        Node node121 = new Node("node121", "Node 1.2.1");
        Node node122 = new Node("node122", "Node 1.2.2");

        node1.addChild(node11);
        node1.addChild(node12);
        node12.addChild(node121);
        node12.addChild(node122);

        Node node2 = new Node("node2", "Node 2");

        nodes.add(node1);
        nodes.add(node2);

        return nodes;
    }

    public List<Node> getNodesForJsTree1() {
        List<Node> nodes = new ArrayList<>();

        Node node1 = new Node("node1", "Node 1");
        Node node11 = new Node("node11", "Node 1.1");
        Node node12 = new Node("node12", "Node 1.2");
        Node node121 = new Node("node121", "Node 1.2.1");
        Node node122 = new Node("node122", "Node 1.2.2");

        node1.addChild(node11);
        node1.addChild(node12);
        node12.addChild(node121);
        node12.addChild(node122);

        Node node2 = new Node("node2", "Node 2");

        nodes.add(node1);
        nodes.add(node2);

        return nodes;
    }

    public List<Node> getNodesForJsTree2() {
        List<Node> nodes = new ArrayList<>();

        Node node3 = new Node("node3", "Node 3");
        Node node31 = new Node("node31", "Node 3.1");
        Node node32 = new Node("node32", "Node 3.2");
        Node node321 = new Node("node321", "Node 3.2.1");
        Node node322 = new Node("node322", "Node 3.2.2");

        node3.addChild(node31);
        node3.addChild(node32);
        node32.addChild(node321);
        node32.addChild(node322);

        Node node4 = new Node("node4", "Node 4");

        nodes.add(node3);
        nodes.add(node4);

        return nodes;
    }

    public Node getNodeById(String id) {
        return null;
    }

}
