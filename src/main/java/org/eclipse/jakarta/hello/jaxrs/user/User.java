package org.eclipse.jakarta.hello.jaxrs.user;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User{
    private Integer id;
    private String username;
    private String password;
    private boolean isAdmin;
}